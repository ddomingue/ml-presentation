## Introduction au Machine Learning

# ( ML 101 )


---
## PLAN

- Autour du machine learning
	- Liens et comparaisons entre domaines
- Définitions et contexte
	- Typologie
	- Domaines d'application
- Ecosystèmes et frameworks
---
## IA & ML

---

### Un sous domaine de l'intelligence artificielle
<br>
@quote[L'ensemble des théories et des techniques mises en œuvre en vue de réaliser des machines capables de simuler l'intelligence]()

---
### Pas si récent que ça..

| Date  | |
|---------|----------------|
| 1666 | "Calculus Raciocinator", Leibniz |
| 1950 | "Computing Machinery and Intelligence", Alan Turing |
| 1962 | MADALINE : Réseau de neurones pour la réduction d'échos (modems) |

Note:
-Leibniz : philosophe, scientifique, mathématicien, logicien, diplomate, juriste, bibliothécaire et philologue allemand du 17eme
-Computing Machinery and Intelligence : paper by Turing, 'Can machines think?', The Turing test -> imitation game
-MADALINE : Bernard Widrow and Ted Hoff, Rule I 1962, Rule II 1988
---

@snap[north-west span-45]
@box[bg-orange text-white box-padding](Symbolique#Systèmes experts<br> Modèles logiques <br> Logique floue)
@snapend

@snap[north-east span-45]
@box[bg-orange text-white box-padding](Statistique#Machine Learning<br> Réseaux de neurones<br> Deep Learning)
@snapend

@snap[south-west span-45]
@box[bg-orange text-white box-padding](Bio-Inspirée#Algos évolutionnistes<br> Algos génétiques)
@snapend

@snap[south-east span-45]
@box[bg-orange text-white box-padding](Autres# Robotique<br>Cybernétique<br>Systèmes multi-agents)
@snapend

@snap[middle span-100]
@box[bg-black text-white box-padding](IA : un domaine large)
@snapend

---

![IA_ML_et_al](images/carto.png =640x480)

---
## Dev ML & Dev Classique

--- 

@snap[west span-40]
@box[bg-purple text-white box-padding](Dev Classique#Implémenter un process métier dans un composant logiciel)
@quote[Programmer un modèle pour faire]
@snapend


@snap[east span-40]
@box[bg-orange text-white box-padding](Dev ML#Implémenter un workflow pour apprendre des représentations/règles sur une tâche)
@quote[Programmer un modèle pour apprendre à faire]
@snapend

---
@snap[north span-49]
@box[bg-white text-black text-11](Cycle de développement)
@snapend

@snap[west span-49]
@box[bg-purple text-white text-07](Dev Classique)
<br>
![DEV](images/classic_approach.png)
@snapend


@snap[east span-49]
@box[bg-orange text-white text-07](Dev ML)
<br>
![ML](images/ml_approach.png)
@snapend

---

### Un paradigme différent

<img src="images/paradigm.jpg" alt="Paradigm" style="width:70%; height:70%" />

---
@snap[west span-20]
@box[bg-blue text-white text-10](Quand privilégier le ML?)
@snapend

@snap[east span-70]
@box[bg-black text-white text-08](Données représentatives du problème en volumétrie suffisante)
<br>
@box[bg-orange text-white text-08](Règles complexes à formaliser ou floues, et beaucoup de réglages pour une fonctionnalité précise)
@box[bg-orange text-white text-08](Un contexte de données qui évolue dans le temps)
@box[bg-orange text-white text-08](Obtenir de l'information à partir de données trop complexes)
@box[bg-orange text-white text-08](Du temps et de l'argent)
@snapend

---

![ML](images/enrico.gif)

---
## ML & Data Science


---

@snap[west span-20]
@box[bg-blue text-white text-10](But)
@snapend

@snap[north-east span-60]
@box[bg-purple text-white box-padding](Data Science#Fournir de la connaissance exploitable sur la donnée)
@snapend

@snap[south-east span-60]
@box[bg-orange text-white box-padding](Machine Learning# Fournir un outil traitant automatiquement de nouvelles données)
@snapend

---

@snap[west span-20]
@box[bg-blue text-white text-10](Utilisation de la donnée)
@snapend

@snap[north-east span-60]
@box[bg-purple text-white box-padding](Data Science#Récupération, traitement, stockage et analyse)
@snapend

@snap[south-east span-60]
@box[bg-orange text-white box-padding](Machine Learning#La donnée est un paramètre d'entrée et sert à la phase d'apprentissage)
@snapend

---

@snap[west span-27]
@box[bg-blue text-white text-10](Statistiques et probabilités)
@snapend

@snap[north-east span-60]
@box[bg-purple text-white box-padding](Data Science#Statistique descriptive et inférentielle <br>Données symboliques et numériques)
@snapend

@snap[south-east span-60]
@box[bg-orange text-white box-padding](Machine Learning#Statistique inférentielle <br>Données uniquement numériques)
@snapend

---

@snap[west span-20]
@box[bg-blue text-white text-10](Outils et Livrables)
@snapend

@snap[north-east span-60]
@box[bg-purple text-white box-padding](Data Science#Outils: Visualisation et Analyse de données<br>Livrables: Supports d'analyses, hypothèses et recommandations)
@snapend

@snap[south-east span-60]
@box[bg-orange text-white box-padding](Machine Learning#Outils: Frameworks, algorithmes et modèles d'apprentissage<br>Livrables: Composant logiciel estimant un mapping A -> B)
@snapend

---

@box[bg-blue text-white text-10](Frontière floue entre Data Science et ML)
<br><br>
![DATA_ML](images/data-scientist_ml-engineer.png)


---
# Définitions et contexte

---


@snap[text-10]
@quote[Machine Learning is the field of study that gives computers the ability to learn without being explicitly programmed.](Arthur Samuel, 1959)
@snapend

Note:
-Pionnier de l'intelligence artificielle, tables de hash, utilisation des transistors pour les pcs, le premier jeu d'échec sur les pcs IBM
---

@snap[text-10]
@quote[A computer program is said to learn from experience E with respect to some task T and some performance measure P, if its performance on T, as measured by P, improves with experience E.](Tom Mitchell, 1997)
@snapend

Note:
-contributions to the advancement of machine learning, artificial intelligence, and cognitive neuroscience and is the author of the textbook Machine Learning
---

- Expérience E : confronter le modèle aux données pendant sa phase d'apprentissage
- Tâche T : l'estimation de la représentation correspondant au problème posé
- Mesure de performance P : déterminer le degré d'adéquation du modèle, notion de coût et d'optimisation

--- 
## Typologie ML

---

@snap[middle span-100]
@box[bg-orange text-white text-20](** Tâche ML : Mapping A -> B**)
@snapend

---

@snap[west span-45]
@box[bg-purple text-white text-10](Associer un label)
<br>
@box[bg-purple text-white text-10](Prédire une valeur)
<br>
@box[bg-purple text-white text-10](Associer à un groupe)
<br>
@box[bg-purple text-white text-10](Explorer/visualiser les données)
@snapend

@snap[east span-45]
@box[bg-orange text-white text-10](Classification)
<br>
@box[bg-orange text-white text-10](Régression)
<br>
@box[bg-orange text-white text-10](Clustering)
<br>
@box[bg-orange text-white text-10](Réduction de dimensionalité)
@snapend

---

### La classification


<img src="images/classification_cat_dog.png" alt="Classification" style="width:70%; height:70%" />

---
### La régression

<img src="images/regression.png" alt="Regression" style="width:60%; height:60%" />

--- 

### Le clustering

<img src="images/kmeans.png" alt="Clustering" style="width:60%; height:60%" />

---
## Méthodes d'apprentissage

---

@snap[west span-45]
@box[bg-purple text-white text-10](Entraînés avec une supervision humaine?)
<br>
@box[bg-purple text-white text-10](Apprentissage avant ou pendant l'exécution?)
<br>
@box[bg-purple text-white text-10](Apprentissage par instance, ou par modèle?)
@snapend


@snap[east span-45]
@box[bg-orange text-white text-10](Apprentissage supervisé / non supervisé)
<br>
@box[bg-orange text-white text-10](Apprentissage en mode batch / en ligne)
<br>
@box[bg-orange text-white text-10](Prédiction par similarité / modèle ML)
@snapend

---
![ML-typology](images/machine-learning-typology.png)

---
### Réseaux de neurones
### Deep Learning

![neurals](images/neurals.jpg)

---
### Pourquoi le Deep Learning?

![perfs](images/nn_perf.jpeg)

---
# Le workflow standard

---
![workflow](images/workflow.jpeg)

---
@snap[north span-90]
@box[bg-orange text-white text-10](Etape 0 : Evaluer le problème)
@snapend

- A-t-on un problème bien défini?
- Quelle est la tâche que l'on souhaite effectuer?
- Est-ce que l'on a un ensemble de données suffisant?
- Est-ce que l'on peut mesurer clairement les performances (métriques)?

---
@snap[north span-90]
@box[bg-orange text-white text-10](Etape 1 : Récupération de la donnée)
@snapend

- Récupérer et agréger les différentes sources de données.
- Visualiser et analyser la données pour extraire des tendances.
- Identifier des caractéristiques exploitables pour le modèle ML.

---
@snap[north span-90]
@box[bg-orange text-white text-10](Etape 2 : Préparation et manipulation de la donnée)
@snapend

- Normalisation de la donnée
- Simplification de la donnée ( élimination des redondances )
- Transformation des données symboliques en numérique 
- Séparation de la donnée pour le ML ( train / test / evaluation )

---
### Exemples (1/3)

![house_prices](images/algos/house_price.png)

---
### Exemples (2/3)

![logistic_titanic](images/algos/logistic_titanic.png)

--- 
### Exemples (3/3)

![clustering_wine](images/algos/clustering_wine.png)

---
@snap[north span-90]
@box[bg-orange text-white text-10](Etapes 3/4/5 : Développement du modèle)
@snapend

- Etape fortement itérative
- Sélectionner le modèle le plus approprié à la problématique et la métrique identifiée.
- Evaluation du modèle sur les données (train / test / validation)
- D'après les évaluations, optimiser le modèle ou en changer

---
@snap[north span-90]
@box[bg-orange text-white text-10](Etape 6 : Déploiement )
@snapend

- Une fois que le modèle est satisfaisant
- Comment utiliser le modèle?
- Comment l'interfacer et l'intégrer dans un ensemble applicatif?

---
@snap[north span-90]
@box[bg-orange text-white text-10](Etape 7 : Maintenance )
@snapend

- Les données peuvent évoluer, le modèle aussi
- Les distributions statistiques vont évoluer
- Réapprendre le modèle et l'ajuster aux tendances de données actuelles

--- 
## L'écosystème ML

---
### Ecosystème python

![frameworks](images/pydata_stack.png)

---
### Frameworks

- Simplifier les tâches sur le worflow
	- Gestion de l'alimentation des données (pipelines)
	- Fournir des implémentations (algos, fonctions de coût, visualisation )
	- Automatisation et parallélisation des calculs (apprentissage et restitution)
	- Sauvegarde, chargement et déploiement des modèles

---
### Frameworks

![frameworks](images/frameworks.png)

---
# Conclusion

[Groupe gitlab LabM](https://gitlab.talanlabs.com/labm)

---
## Difficultés

- quantités insuffisantes de données
- données non représentatives du problème à résoudre
- données de mauvaise qualité
- informations hors-contexte
- overfitting (variance)
- underfitting (bias)


---
### Un panorama

![ML](images/ml_map.png)

--- 
## ML 102
### La régression linéaire simple à 1 dimension

- Apprentissage supervisé
	- Mode batch
	- Régression

[Lien Google Colab](https://drive.google.com/open?id=1AOXwAt_HMCmOCRXjY4_BdU4toqgmShR_)

---
## ML103
### La régression logistique multidimensions

- Apprentissage supervisé
	- Mode batch
	- Classification

[Lien Google Colab](https://drive.google.com/open?id=15000DW3sym8RMBu7JcQwkDEiMSlejvXU)

